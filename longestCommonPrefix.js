const LongestCommonPrefix = (strArr = []) => {
  if (strArr.length === 0) return "";

  // ["flower", "flow", "flight"];
  // flow
  // strArr[0][0] = flower => f
  // strArr[0][1] = flower => l
  // strArr[0][2] = flower => o
  // strArr[0][3] = flower => w

  // strArr[1][0] = flower => f
  // strArr[1][1] = flower => l
  // strArr[1][2] = flower => o
  // strArr[1][3] = flower => w

  // strArr[2][0] = flower => f
  // strArr[2][1] = flower => l
  // strArr[2][2] = flower => i
  // strArr[2][3] = flower => g

  // if  char first  === strArr[0][0]
  // check is match on every char on multiple arr
  // set  char first concat result
  // f + l

  const strArrSort = strArr
    .map((s) => {
      const charArray = s.split("");
      return charArray;
    })
    .sort((a, b) => a.length - b.length);

  const firstArr = strArrSort[0];
  const result = firstArr.filter((firstChar, i) => {
    const isMatch = strArrSort.every((arr) => arr[i] === firstChar);
    return isMatch && firstChar;
  });

  return result.join("");
};

module.exports = { LongestCommonPrefix };
