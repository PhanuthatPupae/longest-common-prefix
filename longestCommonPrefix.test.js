const { LongestCommonPrefix } = require("./longestCommonPrefix");

describe("LongestCommonPrefix", () => {
  it("largest number", () => {
    expect(LongestCommonPrefix(["flower", "flow", "flight"])).toEqual("fl");
    expect(LongestCommonPrefix(["dog", "racecar", "car"])).toEqual("");
    expect(LongestCommonPrefix(["app", "a", "ant"])).toEqual("a");
  });

  it("empty array", () => {
    expect(LongestCommonPrefix([])).toEqual("");
  });
});
